
/**
 *
 * @author noamn
 */

// מייצג את ערכי שורה אחת בטבלה
public class Category {
     private int mCategoryID;
     private String mCategoryName;
     private String mDescription;
     private byte[] mPicture;
     
     public int getCategoryID()
    {
        return mCategoryID;
    }
    public void setCategoryID(int value)
    {
      mCategoryID = value;  
    }
     public String getCategoryName()
    {
        return mCategoryName;
    }
    public void setCategoryName(String value)
    {
      mCategoryName = value;  
    }
    public String getDescription()
    {
        return mDescription;
    }
    public void setDescription(String value)
    {
      mDescription = value;  
    }
        public byte[] getPicture()
    {
        return mPicture;
    }
    public void setPicture(byte[] value)
    {
      mPicture = value;  
    }
    public void setAll(int id, String name, String info, byte[] pic)
    {
        mCategoryID = id;
        mCategoryName = name;
        mDescription = info;
        mPicture = pic;
    }
}
