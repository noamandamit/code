
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
/**
 * @author noamn
 */
public class TheMainClass {
    /*
    * שימו לב
    * Categories לפני ביצוע התוכנית הוספתי רשומה תשיעית לטבלה
    * זאת כדי לא לשנות את הרשומות המקוריות במהלך הקוד
    *
    * תוכן הרשומה לא משנה
    */

        public static void main(String[] args) {
        List<Category> CategoryList = null;
        SqlServerConnection SQL_CON = new SqlServerConnection();
        // אובייקט מהמחלקה שיצרנו לתקשורת עם השרת
        
        Category tmpCategory = new Category();
        tmpCategory.setAll(9,"Some name", "Test", null);
        
        try
        {
        CategoryList = SQL_CON.GetListOfCategoryObjects(0); // קבלת רשימה מהטבלה
        CategoryList.set(8, tmpCategory); // כלומר האיבר התשיעי  8 שינוי האיבר עם האינדקס 
        SQL_CON.updateCategories(CategoryList); // שינוי הטבלה בבסיס הנתונים עם הרשימה המעודכנת
        }
        catch(Exception e){
            System.out.println("Exeption: " + e);
        }
        
        if(CategoryList != null){
           for (Category item : CategoryList){ // הדפסת כל איברי הרשימה
               System.out.println("CategoryID:    "+item.getCategoryID());
               System.out.println("CategoryName:  "+item.getCategoryName());
               System.out.println("Description:   "+item.getDescription());
               System.out.println("CategoryID:    "+item.getPicture());
               System.out.println("\n");
           }
        }
        
        
        
        try{
            FileOutputStream stream = new FileOutputStream("‪D:\\w\\p.png");
            try {
                stream.write(CategoryList.get(3).getPicture());
            } finally {
                stream.close();
            }
        }
        catch(Exception e){
            System.out.println("Problem: " + e);
        }
        System.out.println("The End!   -   Good night.");    
    }
}
