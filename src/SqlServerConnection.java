
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author noamn
 */
public class SqlServerConnection {
    String userName = "Game_User";
    String password = "12345";
    String dbName = "NORTHWND";
    
    
    // לקבל קישור לבסיס נתונים
    public Connection getConnection() throws Exception {
        String url = "jdbc:sqlserver://DESKTOP-RCEHUJ1;databaseName="+dbName;
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        Connection conn;
        conn = DriverManager.getConnection(url,userName,password);
        return conn;
    }
    
    
    //  numberOfLines להחזיר רשימה של הקטגוריות הראשונות כמספר
    // אפס משמעותו להביא את כל הקטגוריות
    public List<Category> GetListOfCategoryObjects(int numberOfLines) throws Exception
    {
        List<Category> CategoryList = new ArrayList<Category>(); // יצירת ואתחול רשימה
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;
        String selectString;

        if(numberOfLines == 0){ // את כל הרשומות
            selectString =
                "SELECT * FROM [NORTHWND].[dbo].[Categories]";
        }
        else{ //רשומות כמספר שקיבלנו
            selectString =
                "SELECT TOP " + numberOfLines
                + " * FROM [NORTHWND].[dbo].[Categories]"; 
        }
        
        try // נפתח בלוק טריי רק כדי לעשות שימוש בפיינלי
        {
           con = getConnection();
           st = con.createStatement();
           rs = st.executeQuery(selectString);
           
           while(rs.next()) // ריצה על התוצאות
           {
               Category tmp = new Category();
               tmp.setCategoryID(rs.getInt("CategoryID"));
               tmp.setCategoryName(rs.getString("CategoryName"));
               tmp.setDescription(rs.getString("Description"));
               tmp.setPicture(rs.getBytes("Picture"));
               
               CategoryList.add(tmp); //הוספה לרשימה 
           }
        }
        // לא עושים תפיסה של שגיאות כי משליכים אותן הלאה לפונקציה שקראה
        finally { // צריך לעשות סגירה של החיבור גם אם נפלנו באקספטשן
            if (st != null){ // כי אם נפלנו בהתחלה זה יהיה נאל
                st.close();
            }
            
            if (con != null){
                if (!con.isClosed()){  // לדוגמה אם נפל החשמל
                    con.close();
                }
            }
        }
        return CategoryList;
    }
   
    
    //Using Prepared Statements
    //https://docs.oracle.com/javase/tutorial/jdbc/basics/prepared.html

    // id שינוי טבלה בבסיס הנתונים על פי ה
    //  של איברי רשימה שאנחנו מקבלים
    public void updateCategories(List<Category> Categories )
        throws SQLException, Exception
    {
        
        Connection con = null;
        PreparedStatement stUpdateLetter = null;
        // נועד לשאילתות שמקבלות משתנים
        // שומר שלא נקבל משתנה שמשנה את המשמעות הרצויה של השאילה
        // לא נרצה שזה יפעיל פקודה זדונית "delete All DB" לדוגמה אם שם המשתמש הוא

        String updateString = 
            "UPDATE [" + dbName + "].[dbo].[Categories]"
                + " SET [CategoryName] = ? "
                + ", [Description] = ? "
                + ", [Picture] = ?"
                + " WHERE [CategoryID] = ?"; 
        // PreparedStatement סימני השאלה בקרוב יתמלאו בערכים על פי התחביר של
        try {
            con  = getConnection();
            stUpdateLetter = con.prepareStatement(updateString);
            for (Category item : Categories) {
                stUpdateLetter.setString(1,item.getCategoryName());
                stUpdateLetter.setString(2,item.getDescription());
                stUpdateLetter.setBytes(3,item.getPicture());
                stUpdateLetter.setInt(4,item.getCategoryID());
                
                stUpdateLetter.executeUpdate();
            }
        }

        finally {
            if (stUpdateLetter != null){ // כי אם נפלנו בהתחלה זה יהיה נאל
                stUpdateLetter.close();
            }
            if (con != null){
                if (!con.isClosed()){  // לדוגמה אם נפל החשמל
                    con.close();
                }
            }    
        }
    }
}

